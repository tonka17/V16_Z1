package com.example.antonija.v16_z1;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemClick;
import butterknife.OnItemLongClick;

public class MainActivity extends Activity {

    @BindView(R.id.bAddQuery) Button bAddQuery;
    @BindView(R.id.etQuery) EditText edQuery;
    @BindView(R.id.lvQuery) ListView lvQuery;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        this.setUpListView();
        this.startHandlingActivity();
    }

    private void startHandlingActivity() {
    }

    private void setUpListView()
    {
        ArrayList<SearchTopic> topics = this.retrieveTopics();
        TopicAdapter adapter = new TopicAdapter(topics);
        this.lvQuery.setAdapter(adapter);
    }

    private ArrayList<SearchTopic> retrieveTopics()
    {
        ArrayList<SearchTopic> topics = new ArrayList<>();
        return topics;
    }


    @OnClick(R.id.bAddQuery)
    public void addQuery()
    {
        String query = this.edQuery.getText().toString();
        SearchTopic topic = new SearchTopic(query);
        TopicAdapter adapter = (TopicAdapter) this.lvQuery.getAdapter();
        adapter.addQuery(topic);
    }


    @OnItemClick(R.id.lvQuery)
    public void searchInternet(int position) {
        SearchTopic topic = (SearchTopic) this.lvQuery.getAdapter().getItem(position);
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_WEB_SEARCH);
        intent.putExtra(SearchManager.QUERY, topic.getTopic());
        if (canBeCalled(intent)) {
            startActivity(intent);
        }
    }

    private boolean canBeCalled(Intent intent)
    {
        PackageManager pm = this.getPackageManager();
        if(intent.resolveActivity(pm)==null)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    @OnItemLongClick(R.id.lvQuery)
    public boolean removeQuery(int position)
    {
        TopicAdapter adapter = (TopicAdapter) this.lvQuery.getAdapter();
        adapter.removeQueryAt(position);
        return true;
    }
}
