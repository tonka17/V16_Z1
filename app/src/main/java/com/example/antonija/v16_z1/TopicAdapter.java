package com.example.antonija.v16_z1;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Antonija on 30/08/2017.
 */

public class TopicAdapter extends BaseAdapter
{
    ArrayList<SearchTopic> _topics;

    public TopicAdapter(ArrayList<SearchTopic> topics) {
        this._topics = topics;
    }

    @Override
    public int getCount() {
        return this._topics.size();
    }

    @Override
    public Object getItem(int position) {
        return this._topics.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        SearchTopicHolder topicHolder;
        if(convertView==null)
        {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.query_item, parent, false);
            topicHolder = new SearchTopicHolder(convertView);
            convertView.setTag(topicHolder);
        }
        else
        {
            topicHolder = (SearchTopicHolder)convertView.getTag();
        }
        SearchTopic topic = this._topics.get(position);
        topicHolder.tvQueryTitle.setText(topic.getTopic());

        return convertView;
    }

    public void addQuery(SearchTopic topic)
    {
        this._topics.add(topic);
        this.notifyDataSetChanged();
    }

    public void removeQueryAt(int position)
    {
        this._topics.remove(position);
        this.notifyDataSetChanged();
    }

    static class SearchTopicHolder
    {
        @BindView(R.id.tvQueryTitle)TextView tvQueryTitle;

        SearchTopicHolder(View view)
        {
            ButterKnife.bind(this, view);
        }
    }
}
